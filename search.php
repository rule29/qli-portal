<?php
  /**
   * The search results template.
   *
   * Used when a search is performed.
   */
  get_header();

  $search_term = get_search_query();

?>

<section class="container search--main-container" style="background: #c3d6e0;">

    <div class="row">
      <div class="col-5">
        <h1 style="margin-top: 200px;">Search Results</h1>
      </div>
      <div class="col-7">
        <div class="search--form-container">
          <?php get_search_form();?>
          <svg version="1.1"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
             x="0px" y="0px" width="41px" height="42px" viewBox="0 0 41 42" style="enable-background:new 0 0 41 42;" xml:space="preserve">
          <style type="text/css">
            .st0{fill:#7F7F7F;}
          </style>
          <defs>
          </defs>
          <g id="XMLID_1_">
            <path id="XMLID_14_" class="st0" d="M17.7,26.4l-1.2-1.2L15.4,24c-0.2-0.2-0.5-0.2-0.7,0c-3.4,3.5-7,7.9-8.7,10
              c-1.7,2.1-3.2,3.4-4.5,4.5c0,0-1,0.7-1.1,0.9c-0.5,0.5-0.5,1.5,0.1,2.1c0.6,0.6,1.5,0.7,2.1,0.1c0.1-0.1,0.9-1.2,0.9-1.2
              c1.1-1.3,2.3-2.8,4.4-4.6c2.1-1.7,6.5-5.4,9.9-8.8C17.9,26.9,17.9,26.6,17.7,26.4z M15,25.7c-2,2.1-4.2,4.7-6.8,8
              c-0.1,0.1-0.2,0.1-0.3,0c-0.1-0.1-0.1-0.2,0-0.3c2.6-3.3,4.9-5.9,6.8-8c0.1-0.1,0.2-0.1,0.3,0c0,0,0.1,0.1,0.1,0.2
              C15,25.6,15,25.7,15,25.7z"/>
            <g id="XMLID_10_">
              <path id="XMLID_11_" class="st0" d="M41,13.9c0,7.7-6.2,13.9-13.9,13.9s-13.9-6.2-13.9-13.9S19.5,0,27.1,0S41,6.2,41,13.9z
                 M27.1,2.1c-6.5,0-11.8,5.3-11.8,11.8s5.3,11.8,11.8,11.8s11.8-5.3,11.8-11.8S33.6,2.1,27.1,2.1z"/>
            </g>
            <g id="XMLID_6_">
              <path id="XMLID_7_" class="st0" d="M32.2,12.9c-1.1,0-1.9-0.9-1.9-1.9s0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S33.2,12.9,32.2,12.9
                z M32.2,9.5c-0.8,0-1.5,0.7-1.5,1.5c0,0.8,0.7,1.5,1.5,1.5c0.8,0,1.5-0.7,1.5-1.5C33.7,10.2,33,9.5,32.2,9.5z"/>
            </g>
            <g id="XMLID_2_">
              <path id="XMLID_3_" class="st0" d="M27.1,24.3c-5.7,0-10.4-4.7-10.4-10.4S21.4,3.5,27.1,3.5s10.4,4.7,10.4,10.4
                S32.9,24.3,27.1,24.3z M27.1,3.9c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S32.7,3.9,27.1,3.9z"/>
            </g>
          </g>
          </svg>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-12" style="background: white; padding-bottom: 80px;">

        <?php // Fake Close button that takes user back to homepage ?>
        <div id="show-nav" class="close-page">
          <span></span>
        </div>

        <?php // Resources By Section ?>
        <div class="row">
          <div class="col-4">
            <h2>
              Filter Resources
            </h2>

            <?php // Categories // ?>
            <div class="search--dropdown">
              <div>
                Select Category <img src="<?php bloginfo('template_url');?>/assets/img/arrow-dropdown.png" alt="">
              </div>
              <ul>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=comfort-zone">
                    Comfort Zone
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=decisions">
                    Decisions
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=compassion">
                    Compassion
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=energy">
                    Energy
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=best-self">
                    Best Self
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=strengths">
                    Strengths
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=communication">
                    Communication
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=purpose">
                    Purpose
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&category_name=lead-better-meetingsbrainstorming">
                    Meetings and Brainstorming
                  </a>
                </li>
              </ul>
            </div>

            <?php // Resource Type // ?>
            <div class="search--dropdown">
              <div>
                Select Resource <img src="<?php bloginfo('template_url');?>/assets/img/arrow-dropdown.png" alt="">
              </div>
              <ul>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&tag=animation">
                    Animation
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&tag=article">
                    Article
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&tag=PDF">
                    PDF
                  </a>
                </li>
                <li>
                  <a href="<?php bloginfo('url');?>/?s=<?php echo $search_term;?>&tag=videos">
                    Video
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-8">
            <?php if ( have_posts() ) : ?>

              <?php while ( have_posts() ) : the_post();

              ?>

                <div class="col-10 col-centered">
                    <?php
                    // Video post Format
                    if(has_post_format('video') || has_post_format('gallery')):
                      get_template_part('parts/post-format-video');
                    else:
                      get_template_part('parts/post-format-download');
                    endif;?>
                    <hr>
                </div>
              <?php
              endwhile; ?>

              <?php //search pagination ?>
              <div class="col-10 text-center col-centered" style="margin-top: 40px;">
                <?php the_posts_pagination( array('mid_size' => 2) ); ?>
              </div>

            <?php else : ?>
              <div class="row">
                <div class="col-10 col-centered" style="margin-top: 40px;">
                  <h2>Sorry, no items have been found.</h2>
                  <p>Try some of these tags instead:</p>
                  <div>
                    <?php wp_tag_cloud();?>
                  </div>
                </div>
              </div>

            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php // Home Nav Include ?>
  <div class="page--home-container">
    <?php get_template_part('parts/home-nav');?>
  </div>

<?php
  get_footer();
