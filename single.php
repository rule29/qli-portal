<?php
  /**
   * The single post template.
   *
   * Used when a single post is queried.
   */
  get_header();
?>

  <section class="container single--container" style="background: #72746F;">
    <div class="row">
      <article class="col-8 col-centered text-center" style="background: white;">

        <?php // Fake Close button that takes user back to homepage ?>
        <div id="show-nav" class="close-page">
          <span></span>
        </div>

        <h2>
          <?php the_title();?>
        </h2>

        <div class="flex-video widescreen vimeo">
          <?php the_field('video_embed_code');?>
        </div>

        <?php
          if( get_field('video_description') ):
              the_field('video_description');
          endif;
        ?>

      </article>
    </div>
  </section>

  <?php // Home Nav Include ?>
  <div class="page--home-container">
    <?php get_template_part('parts/home-nav');?>
  </div>
<?php
  get_footer();
