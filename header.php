<?php include('password.php'); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <?php endif; ?>
  <?php if (is_search()) { ?>
   <meta name="robots" content="noindex, nofollow" />
	<?php } ?>

  <meta property="og:title" content="<?php the_title(); ?>" />
  <meta property="og:site_name" content="<?php bloginfo('name') ?>">

  <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <div class="animsition" data-animsition-overlay="false">

  <header class="container container--justify-content-center">
    <div class="row">
      <div class="sm-col-6 col-1">
        <a href="#" id="logo-nav-open">
          <h1>
            Quiet Rev Ambassador Portal
          </h1>
        </a>
      </div>
      <?php //mobile hamburger ?>
      <div class="sm-col-4 text-center small-only">
        <a id="nifty-nav-toggle"><span></span></a>
      </div>
      <?php //desktop nav ?>
      <div class="col-11">
        <nav>
          <?php wp_nav_menu(array('theme_location' => 'header'));?>
        </nav>
        <div class="header--search-input">
          <?php get_search_form();?>
        </div>
        <div class="header--search-container">
          <svg version="1.1"
          	 xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
          	 x="0px" y="0px" width="41px" height="42px" viewBox="0 0 41 42" style="enable-background:new 0 0 41 42;" xml:space="preserve">
          <style type="text/css">
          	.st0{fill:#7F7F7F;}
          </style>
          <defs>
          </defs>
          <g id="XMLID_1_">
          	<path id="XMLID_14_" class="st0" d="M17.7,26.4l-1.2-1.2L15.4,24c-0.2-0.2-0.5-0.2-0.7,0c-3.4,3.5-7,7.9-8.7,10
          		c-1.7,2.1-3.2,3.4-4.5,4.5c0,0-1,0.7-1.1,0.9c-0.5,0.5-0.5,1.5,0.1,2.1c0.6,0.6,1.5,0.7,2.1,0.1c0.1-0.1,0.9-1.2,0.9-1.2
          		c1.1-1.3,2.3-2.8,4.4-4.6c2.1-1.7,6.5-5.4,9.9-8.8C17.9,26.9,17.9,26.6,17.7,26.4z M15,25.7c-2,2.1-4.2,4.7-6.8,8
          		c-0.1,0.1-0.2,0.1-0.3,0c-0.1-0.1-0.1-0.2,0-0.3c2.6-3.3,4.9-5.9,6.8-8c0.1-0.1,0.2-0.1,0.3,0c0,0,0.1,0.1,0.1,0.2
          		C15,25.6,15,25.7,15,25.7z"/>
          	<g id="XMLID_10_">
          		<path id="XMLID_11_" class="st0" d="M41,13.9c0,7.7-6.2,13.9-13.9,13.9s-13.9-6.2-13.9-13.9S19.5,0,27.1,0S41,6.2,41,13.9z
          			 M27.1,2.1c-6.5,0-11.8,5.3-11.8,11.8s5.3,11.8,11.8,11.8s11.8-5.3,11.8-11.8S33.6,2.1,27.1,2.1z"/>
          	</g>
          	<g id="XMLID_6_">
          		<path id="XMLID_7_" class="st0" d="M32.2,12.9c-1.1,0-1.9-0.9-1.9-1.9s0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S33.2,12.9,32.2,12.9
          			z M32.2,9.5c-0.8,0-1.5,0.7-1.5,1.5c0,0.8,0.7,1.5,1.5,1.5c0.8,0,1.5-0.7,1.5-1.5C33.7,10.2,33,9.5,32.2,9.5z"/>
          	</g>
          	<g id="XMLID_2_">
          		<path id="XMLID_3_" class="st0" d="M27.1,24.3c-5.7,0-10.4-4.7-10.4-10.4S21.4,3.5,27.1,3.5s10.4,4.7,10.4,10.4
          			S32.9,24.3,27.1,24.3z M27.1,3.9c-5.5,0-10,4.5-10,10s4.5,10,10,10s10-4.5,10-10S32.7,3.9,27.1,3.9z"/>
          	</g>
          </g>
          </svg>

        </div>
      </div>
    </div>

    <?php // About and Contact Panels ?>
    <div class="header--panel">
      <div class="close-page">
        <span></span>
      </div>
      <div class="container container--justify-content-center about--panel">
        <div class="row">
          <div class="col-8 col-centered">
            <?php the_field('about_content', 'option');?>
          </div>
        </div>
      </div>
      <div class="container container--justify-content-center contact--panel">
        <div class="row">
          <div class="col-8 col-centered">
            <?php the_field('contact_content', 'option');?>
          </div>
        </div>
      </div>
    </div>

  </header>
  <?php // Nifty Nav Panel ?>
  <div class="nifty-panel container">
    <div class="row">
      <div class="sm-col-12">
        <nav>
          <?php wp_nav_menu(array('theme_location' => 'header'));?>
        </nav>
        <div class="header--search-input">
          <?php get_search_form();?>
        </div>
      </div>
    </div>
  </div>

  <?php // Main Content ?>
  <main class="main--container">
