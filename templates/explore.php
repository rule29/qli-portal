<?php
  /* Template Name: Explore / Standard Page */
  get_header();
?>

  <section class="container single--container" style="background: #72746F;">
    <div class="row">
      <article class="col-8 col-centered text-center" style="background: white;">

        <?php // Fake Close button that takes user back to homepage ?>
        <div id="show-nav" class="close-page">
          <span></span>
        </div>
        <?php if(have_posts()): while(have_posts()): the_post();?>
        <h2>
          <?php the_title();?>
        </h2>

        <div>
          <?php the_content();?>
        </div>

        <?php endwhile; endif;?>

      </article>
    </div>
  </section>

  <?php // Home Nav Include ?>
  <div class="page--home-container">
    <?php get_template_part('parts/home-nav');?>
  </div>
<?php
  get_footer();
