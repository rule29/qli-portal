Key project outline ================================================
- Overall project design inspiration: http://minimalmonkey.com/
- Secured site with a simple modal overlay, and shared user / pass. 
- These 5 verticals are pages. These pages have settings like titles and colors. 
- On each vertical, there are 10 total categories of content. These are actually divided into 2 blocks. a 4/6 split.  
- These content categories are post based. 
- We "cluster" content by post format (videos, audio files, PDFs, presentations and images). Newest first inside of each cluster. 
- Posts will be tagged to match the vertical. This way help for me only shows posts that match the tag help for me. 
- Post format will drive the post interface elements. Videos will ask for a vimeo ID, PDFs will be a title, button text, and PDF upload / URL. Etc. 
- Animation on the home screen will follow this model: 
	http://codepen.io/erwstout/pen/ZWOLOP
- Click through to a post will use a tool like: http://git.blivesta.com/animsition/slide-top/
- We will use the home as the main navigation, present on all pages in code, but reveled when a user clicks the close or menu button. 
- All tools page will be posts by category or post format only. Newest first, not clustered by post format in an archive. 
- Agenda builder will be a later part of the project (offline for now). 




Post format / content types ================================================
- videos, animations and downloads.  Downloads further broken down into tips/tools, exercises, presentations, and other resources (research papers, etc.) 

The above ^ list is in order of the output as well. 


Other output notes ================================================
- We were thinking for videos and animations having a description underneath the still and and for all the PDF downloads having a mouse over pop up description.  




Pages ================================================

- Built on 5 tagged verticals. Each page ONLY shows posts that match the tag for that vertical. 
- Each page has the hard coded category anchor links (Using a smooth Scroll to) on the left. 
- Page features the page title and intro copy above the posts.
- Page ACF includes Options for Primary and secondary color sets. 
- Page body BG color is pulled from the ACF primary color
- Home menu column BG color is pulled from the Page ACF colors (normal state: secondary, hover: primary)

- Page query for categories is already built by James, but will need customized for the final tags, categories and clusters.
- Download post format titles will need a tool tip (example shown was the foundation tooltip) that pulls the description from the download post. Hover to a mouse pointer finger. 

- X icon in the top right will reveal the main navigation for selecting the 5 verticals ON this page. Not a link back to home. 



Posts ================================================
- Posts have Tags, Categories and Post Formats
- ACF options per post will be driven by the post_format

* Videos: Title, iframe embed, and description
* Downloads: Title, File link, Description (loaded as a tool tip) and button CTA text


All tools page  ================================================
- This will be posts by all, category or post format only. Newest first, not clustered by post format in an archive. 