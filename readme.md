# Quiet Rev Ambassador Portal
A custom WordPress theme by Rule29 and Factor1.

## Packages Used
- [Prelude](https://github.com/factor1/prelude) by Factor1
- [Ginger](https://github.com/erwstout/ginger) by Eric Stout
- [Animsition](https://github.com/blivesta/animsition) by blivesta
- [Sugar](https://github.com/erwstout/sugar) by Eric Stout/Factor1

## Installing Locally
To get up and running clone this repo and run `npm install && bower install`. To view a list of Gulp tasks available use: `$ gulp -T` or view the Prelude documentation for a break down of tasks and functions available.

## Remote Repositories
The remote repository addresses for this project are:

Staging:
```
ssh://ther29@ther29.com/home/ther29/repo/qli-portal.git
```
Production:
```
ssh://quiettoolkit@52.37.240.85/home/quiettoolkit/repos/qli-portal.git
```

## Questions?
Contact factor1 at http://factor1studios.com or support@factor1.org
