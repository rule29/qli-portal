<?php
  /**
   * The default page template.
   *
   * Used when a default template individual page is queried.
   */
  get_header();

  //grab page slug for query
  global $post;
  $post_slug = $post->post_name;

  // part of custom query
  $panels = get_panels_content([
      $post_slug
  ],[
      'achieve-self-awareness',
      'develop-compassion-and-understanding',
      'know-your-strengths',
      'define-your-purpose-be-your-best-self',
      'step-outside-your-comfort-zone',
      'manage-your-energy',
      'communicate-with-presence-and-influence',
      'transform-how-you-work',
      'enrich-creative-and-innovative-practices',
      'lead-authentically'
  ]);

?>


<section class="container" style="background: <?php the_field('page_color');?>;">
  <div class="row">
    <?php // PAGE SIDEBAR ?>
    <aside class="col-4">
      <?php get_template_part('parts/sidebar');?>
    </aside>

    <?php // MAIN CONTENT ?>
    <?php foreach($panels as $tag_slug => $categories) { $tag = get_term_by('slug',$tag_slug,'post_tag'); ?>
      <article class="col-8 text-center">
        <?php // Fake Close button that takes user back to homepage ?>
        <div id="show-nav" class="close-page">
          <span></span>
        </div>

        <?php // CONTENT INTRO
        $i = 0;
        ?>
        <?php foreach($categories as $category_slug => $post_formats) { $category = get_term_by('slug',$category_slug,'category'); $i++; ?>
          <div id="<?php echo esc_attr($category->slug); ?>" class="content--intro content-<?php echo $i;?>">
            <h2>
              <?php echo esc_html($category->name); ?>
            </h2>
            <p class="sub-head">
              <?php echo esc_html($category->description); ?>
            </p>
          </div>

          <?php
          foreach($post_formats as $post_format => $posts) {
           foreach($posts as $post) {
             // detect the post formats
             if( $post_format == 'chat'){
              include('parts/post-format-download.php');
             }
             else if ( $post_format == 'video' || $post_format == 'gallery' ){
               // if it's a video or animation
               include('parts/post-format-video.php');
             } else{
               include('parts/post-format-download.php');
             }
           }
          }
        } ?>
      </article>
    <?php } ?>
  </div>
</section>

<?php // Home Nav Include ?>
<div class="page--home-container">
  <?php get_template_part('parts/home-nav');?>
</div>

<?php
  get_footer();
