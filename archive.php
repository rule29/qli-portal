<?php
  /**
   * The archive template.
   *
   * Used when a category, author, or date is queried.
   */
  get_header();
?>

  <section class="container archive--container" style="background: #72746F;">
    <div class="row">
      <div class="col-8 col-centered" style="background: white;">

        <?php if ( is_category() ) : ?>
          <h2>Archive for the <?php single_cat_title(); ?> Category</h2>
        <?php elseif ( is_tag() ) : ?>
          <h2>Posts Tagged <?php single_tag_title(); ?></h2>
        <?php elseif ( is_day() ) : ?>
          <h2>Archive for <?php the_time( 'F jS, Y' ); ?></h2>
        <?php elseif ( is_month() ) : ?>
          <h2>Archive for <?php the_time( 'F, Y' ); ?></h2>
        <?php elseif ( is_year() ) : ?>
          <h2 class="pagetitle">Archive for <?php the_time( 'Y' ); ?></h2>
        <?php elseif ( is_author() ) : ?>
          <h2 class="pagetitle">Author Archive</h2>
        <?php elseif ( isset($_GET[ 'paged' ]) && !empty($_GET[ 'paged' ]) ) : ?>
          <h2 class="pagetitle">Blog Archives</h2>
        <?php endif; ?>

        <?php
        if(have_posts()):
          while(have_posts()):
            the_post();
            if( has_post_format('video') || has_post_format('gallery')):
              include('parts/post-format-video.php');
            else:
              include('parts/post-format-download.php');
            endif;
          endwhile;
        endif;
        ?>
        <div class="row">
          <div class="col-10 col-centered text-center" style="margin-bottom: 20px;">
            <?php the_posts_pagination( array('mid_size' => 2) ); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php // Home Nav Include ?>
  <div class="page--home-container">
    <?php get_template_part('parts/home-nav');?>
  </div>

<?php
  get_footer();
