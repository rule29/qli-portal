<?php // Rename Post Formats


/*
WP name  ==  rename

Chat == playbook
Video == videos
Gallery == animations
Downloads == downloads
Aside == tips/tools
Status == exercises
Audio == presentations
Quote == other resources
*/

function rename_post_formats( $safe_text ) {
    if ( $safe_text == 'Gallery' )
        return 'Animations';

    if ( $safe_text == 'Aside' )
        return 'Tips and Tools';

    if ( $safe_text == 'Status' )
        return 'Exercises';

    if ( $safe_text == 'Audio' )
        return 'Presentations';

		if ( $safe_text == 'Chat' )
				return 'Playbook';

    if ( $safe_text == 'Quote' )
        return 'Other Resources';

    return $safe_text;
}
add_filter( 'esc_html', 'rename_post_formats' );

//rename Aside in posts list table
function live_rename_formats() {
    global $current_screen;

    if ( $current_screen->id == 'edit-post' ) { ?>
        <script type="text/javascript">
        jQuery('document').ready(function() {

            jQuery("span.post-state-format").each(function() {
                if ( jQuery(this).text() == "Gallery" )
                    jQuery(this).text("Animations");

                if ( jQuery(this).text() == "Aside" )
                    jQuery(this).text("Tips and Tools");

                if ( jQuery(this).text() == "Status" )
                    jQuery(this).text("Exercises");

                if ( jQuery(this).text() == "Audio" )
                    jQuery(this).text("Presentations");

                if ( jQuery(this).text() == "Quote" )
                    jQuery(this).text("Other Resources");

								if ( jQuery(this).text() == "Chat" )
                    jQuery(this).text("Playbook");
            });

        });
        </script>
<?php }
}
add_action('admin_head', 'live_rename_formats');
