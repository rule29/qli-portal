<?php

/**
*   Returns an array of posts for a panel based on the tag slug
*   and an array of category slugs to query.
*
*   @param string $tag
*   @param array $categories
*   @param interger $post_limit
*
*   @return array
*/
function get_panel_posts($tag, array $categories, $post_limit = 10) {
    if(!is_string($tag) || !$categories) {
        return [];
    }

    $posts = [];

    foreach($categories as $category_name) {

        $category = get_term_by('slug',$category_name,'category');

        // Slug not found, skip
        if(!$category) {
            continue;
        }

        $key = $category->slug;

        // Load category posts
        $category_posts = get_posts([
            'post_type' => 'post',
            'posts_per_page' => $post_limit,
            'tag' => $tag,
            'tax_query' => [[
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' => $category->slug,
            ]]
        ]);

        // No posts found, skip
        if(!$category_posts) {
            continue;
        }

        // Prepare posts by category and post format
        foreach($category_posts as $post) {

            if(!isset($posts[$key])) {
                $posts[$key] = [

                    // Set included and order of post formats here
                    'chat' => [],
                    'video' => [],
                    'gallery' => [],
                    'aside' => [],
                    'status' => [],
                    'audio' => [],
                    'quote' => [],

                ];
            }

            // Load the post format
            $post_format = get_post_format($post);

            if(!$post_format) {
                $post_format = 'standard';
            }

            if(!isset($posts[$key][$post_format])) {
                // If not defined above, skip
                continue;
                //$posts[$key][$post_format] = [];
            }

            $posts[$key][$post_format][] = $post;

        }

        // Filter out empty sections
        $posts[$key] = array_filter($posts[$key],function($a) {
            return (count($a)>0);
        });
    }

    return $posts;
}

/**
*   Returns an array of posts for all panels with the same
*   categories. Use get_panel_posts if you want different categories
*   per panel.
*
*   @param $tags
*   @param $categories
*
*   @return array
*/
function get_panels_content(array $tags,array $categories) {
    if(!$tags || !$categories) {
        return [];
    }

    $posts = [];

    foreach($tags as $tag) {

        $panel_posts = get_panel_posts($tag,$categories);

        if(!$panel_posts) {
            continue;
        }

        $posts[$tag] = $panel_posts;
    }

    return $posts;
}
