// variables for scripts
$search       = $('.header--search-container');
$searchInput  = $('.header--search-input input[type=search]');
$body         = $('body');

// ------ Stick Sidebar -----
if( $(window).width() > 688 ){
  $('.sidebar--container').stickUp({
      scrollHide:false,
      topMargin: '200px'
  });
}

// ------- Waypoints --------

if( $(window).width() > 688 ){
  var categoryWaypoint = function(){

    $('aside.col-4 h3').click(function(){
      $this = $(this);
      if( $this.html() == 'Application' ){
        $('#application li').fadeToggle();
        $('#building-blocks li').fadeToggle();
      } else{
        $('#building-blocks li').fadeToggle();
        $('#application li').fadeToggle();
      }

    });

    $('.content--intro').each(function(){

      var $id = $(this).attr('id'),
          $idWaypoint = $id,
          buildingBlocks = [
            'achieve-self-awareness',
            'develop-compassion-and-understanding',
            'know-your-strengths',
            'define-your-purpose-be-your-best-self',
          ],
          application = [
            'step-outside-your-comfort-zone',
            'manage-your-energy',
            'communicate-with-presence-and-influence',
            'transform-how-you-work',
            'enrich-creative-and-innovative-practices',
            'lead-authentically'
          ];

      $idWaypoint = new Waypoint.Inview({
        element: document.getElementById($id),
        entered: function(direction) {
          if( jQuery.inArray($id, buildingBlocks) !== -1 ){
            $('#building-blocks li').fadeIn();
            $('#application li').fadeOut();
          } else if(jQuery.inArray($id, application) !== -1){
            $('#application li').fadeIn();
            $('#building-blocks li').fadeOut();
          }
          $('a.' + $id).addClass('active');
        },
        exited: function(direction) {
          $('a.' + $id).removeClass('active');
        }
      });

    });
  }; categoryWaypoint();
}

jQuery( document ).ready(function( $ ) {

  // ------------ header search toggle ------------
  $search.click(function(){
    $searchInput.toggleClass('search-active');
    setTimeout(function(){
      $('header .header--search-input .search-field').focus();
    }, 500);
  });

  // ---------- Nifty Nav -------------------------
  if( $body.hasClass('home') ){
    niftyNav({
      panelPosition: 'fixed'
    });
  } else{
    niftyNav();
  }

  // ----------- animsition scripts ----------------
  $('.animsition').animsition({
    inClass: 'fade-in',
    outClass: 'fade-out',
    inDuration: 1500,
    outDuration: 800,
    linkElement: 'a:not([target="_blank"]):not([href^=#]):not([id^=nifty])',
    // e.g. linkElement: 'a:not([target="_blank"]):not([href^=#])'
    loading: true,
    loadingParentElement: 'body', //animsition wrapper element
    loadingClass: 'animsition-loading',
    loadingInner: '', // e.g '<img src="loading.svg" />'
    timeout: false,
    timeoutCountdown: 5000,
    onLoadEvent: true,
    browser: [ 'animation-duration', '-webkit-animation-duration'],
    // "browser" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
    // The default setting is to disable the "animsition" in a browser that does not support "animation-duration".
    overlay : false,
    overlayClass : 'animsition-overlay-slide',
    overlayParentElement : 'body',
    transition: function(url){ window.location.href = url; }
  });

  // ------- Smooth Scroll -------------------
  $('.smooth-scroll').click(function(){
      $('html, body').animate({
          scrollTop: $( $(this).attr('href') ).offset().top - 95
      }, 500);
      return false;
  });

  // ------- Click X or hit Escape to show Home nav ---------
  if( $('body').hasClass('single-post') ){
    $('#show-nav, #logo-nav-open').click(function(){
      window.history.back();
    });

    $(document).keyup(function(e) {
       if (e.keyCode == 27) {
         window.history.back();
      }
    });
  } else{
    $('#show-nav, #logo-nav-open').click(function(){
      $('.page--home-container').slideToggle();
      $('body').toggleClass('locked');
    });

    $(document).keyup(function(e) {
       if (e.keyCode == 27) {
         $('.page--home-container').slideToggle();
         $('body').toggleClass('locked');
      }
    });
  }


  // Fire Header Panels
  headerPanels();

});
