var $headerPanel = $('.header--panel');
    $about = $('#menu-item-15 a');
    $aboutPanel = $('.about--panel');
    $contact = $('#menu-item-17 a, .contact--404');
    $contactPanel = $('.contact--panel');
    panelOpen = false;

// Open panel function
var openPanel = function(){
  $headerPanel.slideDown();
  $('.animsition-overlay').append('<div class="mask"></div>');
  panelOpen = true;
}

// Close Panel function
var closePanel = function(){
  $headerPanel.slideUp();
  $('.mask').remove();
  panelOpen = false;
}

var headerPanels = function(){
  // replace urls of wp3 menus
  $about.attr('href', '#');
  $contact.attr('href', '#');

  // about menu item click
  $about.click(function(){
    if( panelOpen === false ){

      $aboutPanel.fadeIn().addClass('active-panel');
      openPanel();

    } else if( panelOpen === true ){

      if( $aboutPanel.hasClass('active-panel') === true ){

        closePanel();
        $aboutPanel.fadeOut().removeClass('active-panel');

      } else{

        $contactPanel.fadeOut().removeClass('active-panel');
        setTimeout(function(){
          $aboutPanel.fadeIn().addClass('active-panel');
        }, 401);

      }
    }
  });

  // contact menu item click
  $contact.click(function(){
    if( panelOpen === false ){

      $contactPanel.fadeIn().addClass('active-panel');
      openPanel();

    } else if( panelOpen === true ){

      if( $contactPanel.hasClass('active-panel') === true ){

        closePanel();
        $contactPanel.fadeOut().removeClass('active-panel');

      } else{

        $aboutPanel.fadeOut().removeClass('active-panel');
        setTimeout(function(){
          $contactPanel.fadeIn().addClass('active-panel');
        }, 401);

      }
    }
  });

  // click on main container thats not header
  $('.close-page').on('click', function(){

    console.log('[f1 panel]: User click on mask.');
    closePanel();
    setTimeout( function(){
      $aboutPanel.fadeOut().removeClass('active-panel');
      $contactPanel.fadeOut().removeClass('active-panel');
    }, 600 );

  });
}
