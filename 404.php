<?php
  /**
   * The 404 Not Found template.
   *
   * Used when WordPress encounters an unknown URL.
   */
  get_header();
?>

  <section class="error-404 container container--justify-content-center">
    <div class="row">
      <div class="col-6 col-centered text-center">
        <h1>
          Oh No!
        </h1>
        <p>
          We couldn't find the page you were looking for.
        </p>
        <p>
          Not to worry. You can always return to the <a href="<?php bloginfo('url');?>/">homepage</a>,
          or use the navigation (above) to help you find what you're looking for.
          If you've arrived here via a broken link or through some kind of site
          error, <a href="#" class="contact--404">please let us know</a>!
        </p>
      </div>
    </div>
  </section>

<?php
  get_footer();
