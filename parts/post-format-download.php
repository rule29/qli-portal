<?php // DOWNLOAD CLUSTER

// Color logic for <p> based on tags
if( has_tag('help-for-me') ):
  $textColor = '#21a7e0';
  $text = 'Help For Me';
elseif( has_tag('group-sessions') ):
  $textColor = '#2a5c7a';
  $text = 'Group Sessions';
elseif( has_tag('mentoring-others') ):
  $textColor = '#998c73';
  $text = 'Mentoring Others';
elseif( has_tag('speeches') ):
  $textColor = '#fa8524';
  $text = 'Speeches';
else:
  $textColor = '#3f3f3f';
endif;

?>
<div class="download--cluster">
  <div class="row">
    <div class="col-8 text-left">
      <div class="download-title">
        <?php if( is_search() || is_archive() ):?>
          <p class="search-tag" style="color: <?php echo $textColor;?>;">
            <?php echo $text;?>
          </p>
        <?php endif;?>
        <a href="<?php the_field('file_download');?>" target="_blank">
          <p>
            <?php echo esc_html(get_the_title($post)); ?>
          </p>
        </a>
        <?php if( get_field('hover_text') ):?>
          <div class="tooltip">
            <?php the_field('hover_text');?>
          </div>
        <?php endif;?>
      </div>
    </div>
    <div class="col-4">
      <a href="<?php the_field('file_download');?>" target="_blank" class="button-download">
        <?php the_field('button_text');?>
      </a>
    </div>
  </div>
</div>
