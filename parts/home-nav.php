<?php // WP_Query arguments

$args = array (
  'post_type'    =>    'page',
  'post__in'     =>     array(22, 18, 20, 24),
  'order_by'     =>     'id',
  'order'        =>     'ASC'
);

// The Query
$query = new WP_Query( $args );
if($query->have_posts()):
  $i = 0;
?>
    <div class="container home--container">
      <?php while($query->have_posts()): $query->the_post();
        $color      = get_field('page_color');
        $hoverColor = get_field('page_hover_color');
        $i++;
        ?>
        <style>
          .no-<?php echo $i;?>{
            background: <?php echo $color;?>;
          }
          .no-<?php echo $i;?>:hover{
            background: <?php echo $hoverColor;?>;
          }
        </style>
        <div class="home--post-container no-<?php echo $i;?>">
          <a href="<?php the_permalink();?>">
            <div>
            <h2>
              <?php the_title();?>
            </h2>
            <?php the_excerpt();?>
            </div>
            <div>
              <p class="text-center">SEE TOOLS <img src="<?php bloginfo('template_url');?>/assets/img/arrow.png" alt="" class="arrow"></p>
            </div>
          </a>
        </div>
      <?php endwhile;?>
    </div>
  <?php endif; wp_reset_postdata();?>
