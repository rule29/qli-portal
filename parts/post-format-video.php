<?php // VIDEO CLUSTER
// Color logic for <p> based on tags
if( has_tag('help-for-me') ):
  $textColor = '#21a7e0';
  $text = 'Help For Me';
elseif( has_tag('group-sessions') ):
  $textColor = '#2a5c7a';
  $text = 'Group Sessions';
elseif( has_tag('mentoring-others') ):
  $textColor = '#998c73';
  $text = 'Mentoring Others';
elseif( has_tag('speeches') ):
  $textColor = '#fa8524';
  $text = 'Speeches';
else:
  $textColor = '#3f3f3f';
endif;
?>
<div class="content--video-cluster container">
  <div class="row">
    <div class="col-8 text-left">
      <?php if( is_search() || is_archive() ):?>
        <p class="search-tag" style="color: <?php echo $textColor;?>;">
          <?php echo $text;?>
        </p>
      <?php endif;?>
      <div class="download-title">
        <a href="<?php the_permalink();?>">
          <p>
            <?php echo esc_html(get_the_title($post)); ?>
          </p>
        </a>
        <?php
          if( get_field('video_description') ):
            echo '<div class="tooltip">';
              the_field('video_description');
            echo '</div>';
          endif;
        ?>
      </div>
    </div>
    <div class="col-4 text-right">
      <a href="<?php the_permalink();?>" class="thumbnail--container">
        <?php
        if( has_post_thumbnail() ):
          the_post_thumbnail('video-thumb');
        else:?>
          <img src="<?php bloginfo('template_url');?>/assets/img/fallback.jpg" alt="">
        <?php endif;?>
      </a>
    </div>
  </div>
</div>
