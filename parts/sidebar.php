<div class="sidebar--container">
  <h1>
    <?php the_title();?>
  </h1>
  <h3 class="white">
    The Building Blocks
  </h3>
  <hr class="white">
  <ul id="building-blocks">
    <li>
      <a href="#achieve-self-awareness" class="smooth-scroll achieve-self-awareness">
        Self-Awareness
      </a>
    </li>
    <li>
      <a href="#develop-compassion-and-understanding" class="smooth-scroll develop-compassion-and-understanding">
        Compassion and Understanding
      </a>
    </li>
    <li>
      <a href="#know-your-strengths" class="smooth-scroll know-your-strengths">
        Strengths
      </a>
    </li>
    <li>
      <a href="#define-your-purpose-be-your-best-self" class="smooth-scroll define-your-purpose-be-your-best-self">
        Purpose and Best Self
      </a>
    </li>
  </ul>

  <h3 class="white">
    Application
  </h3>
  <hr class="white">
  <ul id="application">
    <li>
      <a href="#step-outside-your-comfort-zone" class="smooth-scroll step-outside-your-comfort-zone">
        Comfort Zone
      </a>
    </li>
    <li>
      <a href="#manage-your-energy" class="smooth-scroll manage-your-energy">
        Energy
      </a>
    </li>
    <li>
      <a href="#communicate-with-presence-and-influence" class="smooth-scroll communicate-with-presence-and-influence">
        Presence and Influence
      </a>
    </li>
    <li>
      <a href="#transform-how-you-work" class="smooth-scroll transform-how-you-work">
        Meetings and Collaboration
      </a>
    </li>
    <li>
      <a href="#enrich-creative-and-innovative-practices" class="smooth-scroll enrich-creative-and-innovative-practices">
        Creativity and Innovation
      </a>
    </li>
    <li>
      <a href="#lead-authentically" class="smooth-scroll lead-authentically">
        Leadership
      </a>
    </li>
  </ul>
</div>
